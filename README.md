# dblog-web-code

#### 介绍
基于 hunt-framework 的 D语言个人博客项目（[https://gitee.com/yuguofu/dblog](https://gitee.com/yuguofu/dblog)）的web前端源码  


#### 软件架构
vue2


#### 安装教程

1.  npm install
2.  npm run serve
3.  npm run build

#### 使用说明

1.  npm install
2.  npm run serve
3.  npm run build

