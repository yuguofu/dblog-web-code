module.exports = {
    // 部署应用包时的基本 URL
    publicPath: "./",
    // 打包(构建)输出路径
    outputDir: "dist",
    // 静态资源目录
    assetsDir: "static",
    devServer: {
        port: 8080,
    },
    lintOnSave: false
}
