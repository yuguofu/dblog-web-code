import request from '@/utils/request'

// 分页获取文章列表
export function fetchPostList(page, perPage) {
    return request({
        url: `/articles?page=${page}&perPage=${perPage}`,
        method: 'get'
    })
}

// 文章详情
export function fetchPostDetail(id) {
    return request({
        url: `/article/${id}`,
        method: 'get'
    })
}

// export function fetchFocus() {
//     return request({
//         url: '/focus/list',
//         method: 'get',
//     })
// }

// 分类列表
export function fetchCategory() {
    return request({
        url: '/categories',
        method: 'get'
    })
}

// 分类详情
export function fetchCategoryDetail(id) {
    return request({
        url: `/category/${id}`,
        method: 'get'
    })
}

// 分类下文章列表
export function fetchArtListByCid(cid) {
    return request({
        url: `/category/${cid}/artlist`,
        method: 'get'
    })
}


// 友链
export function fetchFriend() {
    return request({
        url: '/friend',
        method: 'get',
    })
}

// 社交信息（个人信息）
export function fetchSocial() {
    return request({
        url: '/social',
        method: 'get',
    });
}

// 网站信息
export function fetchSiteInfo() {
    return request({
        url: '/site',
        method: 'get',
    })
}

// 评论
export function fetchComment() {
    return request({
        url: '/comment',
        method: 'get',
    })
}

// 搜索文章
export function searchArticle(words) {
    return request({
        url: `/search/${words}`,
        method: 'get'
    })
}
