import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/css/style.less'
import './assets/font/iconfont.css'
import { parseTime } from './utils'



import hljs from 'highlight.js'
import 'highlight.js/styles/atom-one-dark.css'
// import 'highlight.js/styles/default.css'
// import 'highlight.js/styles/monokai-sublime.css'
// import 'highlight.js/styles/atom-one-dark.css'



Vue.config.productionTip = false;

Vue.filter('parseTime', (v) => parseTime(v, '{y}-{m}-{d}'));


//自定义一个代码高亮指令
Vue.directive('highlight', function (el) {
    let highlight = el.querySelectorAll('pre code');
    highlight.forEach((block) => {
        hljs.highlightBlock(block);
    });
});


new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');


